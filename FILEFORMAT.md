# VisiCalc File Format

**This documentation is incomplete. If you can help complete it, please do!**

A `.VC` file is a simple text file which contains VisiCalc commands. VisiCalc reads in these files by processing them as if they were entered by a keyboard. (However, note that these files are **not** necessarily the same keystrokes that produced the file in the first place.)

A more or less complete summary of VisiCalc syntax can be found on the reference cards available in [the History section of VisiCalc creator Dan Bricklin's website](http://www.bricklin.com/history/refcard1.htm), which also has a [DOS version of VisiCalc](http://www.bricklin.com/history/vcexecutable.htm) which is "very similar" to the original Apple II VisiCalc, and can be used in conjunction with emulation software like DOSBox to create test files or to inspect files in their original environment. PDF scans of original VisiCalc manuals for various platforms are also available in various places on the Internet.

This documentation should be read in conjunction with the parser grammars that can be found in `vc2xlsx/doc_parser.py` and `vc2xlsx/cell_parser.py`, respectively.

## VisiCalc files

### Filling Cell Values

The bulk of a VisiCalc file alternates between the Go To Cell command (`goto_command`) and cell entry (`entry_command`). Despite what the quick reference says, Go To Cell seems to always be terminated by a `:`, not a newline. Thus, the first part of the file will look like this:

	>A1:123
	>A2:456
	>B1:+A1+A2

Cell values will always be in this format, even if they were entered using another method, e.g. navigating to the cell with arrow keys or using the Replicate command.

### Processing Commands

The following commands shouldn't appear as output in .VC files:

	/B /C /D[RC] /I[RC] /M /P /R /S /V

The following may, but can be safely ignored:

	/GO[RC] /GR[AM] /W[HV1SU]

The following are significant:

	/F[DGI$LR*] /GC[number] /GF[GI$LR*] /T[HVBN]

Unknown:

	/-
	
| `/F[DGI$LR*]` | Sets the format of the current cell. See *Format Types* below. |
|
| `/T[HVB]` | Sets the current row and all above to title rows (`H`), the current column and all those to the left to title columns (`V`), or both (`B`) |
| `/TN` | Resets to 0 title rows & columns |
	
#### Format Types

- `$` **Currency**. Round the value to 2 decimal places. (Doesn't display any currency symbols.)
- `*` **Graph**. Round the value to the nearest integer, then display that many stars.

#### The Undocumented /X Command

.VC files seem to always contain, at the very end, a /X command followed by a series of Go To Cell commands. It is not known what the purpose of this command is, but it doesn't seem to affect the output.

Bob Frankston's article says this:

> We saved the spreadsheet in a format that allowed us to use the keyboard input processor to read the file. There were undocumented commands that allowed us to set the initial value of a cell and control the loading. They would also work from the keyboard.



## VisiCalc cell values

### Cell References

Cell references take the form of letters followed by numbers. There are no "absolute" cell references in VisiCalc.

### Functions

As far as I can tell, the function list on the reference card is an exhaustive list of the functions VisiCalc provides.
